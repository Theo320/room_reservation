/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SallesService } from './salles.service';

describe('Service: Salles', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SallesService]
    });
  });

  it('should ...', inject([SallesService], (service: SallesService) => {
    expect(service).toBeTruthy();
  }));
});
