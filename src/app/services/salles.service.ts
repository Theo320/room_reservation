import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reservation } from '../models/reservation';
import { Salle } from '../models/salle';

@Injectable({
  providedIn: 'root'
})
export class SallesService {
  public endpoint = "http://localhost:8080/api/"

constructor(private http: HttpClient) { }

public getSalles(): Observable<Salle[]>{
  let url = this.endpoint + "salles";

  return this.http.get<Salle[]>(url);
}

public getSallesReservations(salleId: number): Observable<Reservation[]>{
  let url = this.endpoint + "salles/" + salleId + "/reservations";

  return this.http.get<Reservation[]>(url);
}

public saveSalle(salle: Salle): any{
  let url = this.endpoint + "salle";
  const body = JSON.stringify(salle);

  return this.http.post(url, body, {headers: 
    {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }}).subscribe();
}

public updateSalle(salle: Salle, salleId: number){
  let url = this.endpoint + "salles/" + salleId;
  const body = JSON.stringify(salle);

  return this.http.put(url, body, {headers: 
    {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }}).subscribe();
}

public deleteSalle(salleId: number){
  let url = this.endpoint + "salles/" + salleId;
  
  return this.http.delete(url, {headers: 
    {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }}).subscribe();
}


}
