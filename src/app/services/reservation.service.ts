import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Reservation } from '../models/reservation';
import { AlertComponent } from '../utils/alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  public endpoint = "http://localhost:8080/api/"


constructor(private http: HttpClient, private _snackBar: MatSnackBar) { }

public saveReservation(salleId: number, reservation: Reservation): any{
  let url = this.endpoint + "salles/"+ salleId + "/reservation";
  const body = JSON.stringify(reservation);

  return this.http.post(url, body,{headers: 
    {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }, observe: 'response'}).subscribe(response => {
      if(response.status === 226){
        this.openSnackBar();
      }
    })
}

public deleteReservation(reservationId: number){
  let url = this.endpoint + "reservations/" + reservationId;
  
  return this.http.delete(url, {headers: 
    {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }}).subscribe();
}

openSnackBar() {
  this._snackBar.openFromComponent(AlertComponent, {
    duration: 3000,
  });
}

}
