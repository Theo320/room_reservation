import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Salle } from 'src/app/models/salle';
import { SallesService } from 'src/app/services/salles.service';
import { AddSalleComponent } from '../add-salle/add-salle.component';
import { ReserverSalleComponent } from '../reserver-salle/reserver-salle.component';

@Component({
  selector: 'card-salle',
  templateUrl: './card-salle.component.html',
  styleUrls: ['./card-salle.component.scss']
})
export class CardSalleComponent implements OnInit {
  @Input() salle!: Salle;

  constructor(private dialog: MatDialog, private salleService: SallesService) { }

  ngOnInit() {
  }

  public openDialog(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      id: this.salle.id
    }

    this.dialog.open(ReserverSalleComponent, dialogConfig);
  }

  delete(salle: Salle){
    console.log(salle.id!)
    if(confirm("Voulez vous vraiment supprimer la salle " + salle.name)){
      this.salleService.deleteSalle(salle.id!);
    }
  }

}
