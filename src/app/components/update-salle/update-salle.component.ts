import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SallesService } from 'src/app/services/salles.service';

@Component({
  selector: 'app-update-salle',
  templateUrl: './update-salle.component.html',
  styleUrls: ['./update-salle.component.scss']
})
export class UpdateSalleComponent implements OnInit, OnDestroy {

  public form!: FormGroup;
  public id!: number;
  public sub: any;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private salleService: SallesService, private router: Router) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: new FormControl('', [Validators.required]),
      capacity: new FormControl('', [Validators.required])
    });

    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    })
  }
  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  onSubmit(){
    if(this.form.valid){
      const salle = {
        name: this.form.get('name')?.value,
        capacity: this.form.get('capacity')?.value,
        image: ''
      }
      this.salleService.updateSalle(salle, this.id);
      this.router.navigate(['/rooms/list'])
    }
  }

}
