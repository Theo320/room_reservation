import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Reservation } from 'src/app/models/reservation';
import { Salle } from 'src/app/models/salle';
import { ReservationService } from 'src/app/services/reservation.service';
import { SallesService } from 'src/app/services/salles.service';

@Component({
  selector: 'app-reservations-list',
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.scss']
})
export class ReservationsListComponent implements OnInit {

  public form!: FormGroup;
  public salles$!: Observable<Salle[]>;
  public reservations$!: Observable<Reservation[]>;
  displayedColumns: string[] = ['description', 'date', 'action'];

  @ViewChild(MatSort) sort!: MatSort;

  constructor(private fb: FormBuilder, private sallesService: SallesService, private reservationService: ReservationService) { }

  ngOnInit() {
    this.form = this.fb.group({
      salle: new FormControl('', [Validators.required]),
    });
    this.salles$ = this.sallesService.getSalles();
  }

  onSubmit(){
    const id = this.form.get('salle')?.value;
    console.log(id)
    this.reservations$ = this.sallesService.getSallesReservations(id);
  }

  deleteRes(data: Reservation){
    if(confirm("Voulez vous vraiment supprimer la réservation du " + data.date)){
      this.reservationService.deleteReservation(data.id!);
      window.location.reload()
    }
  }

}
