import { DatePipe, formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Reservation } from 'src/app/models/reservation';
import { ReservationService } from 'src/app/services/reservation.service';

@Component({
  selector: 'reserver-salle',
  templateUrl: './reserver-salle.component.html',
  styleUrls: ['./reserver-salle.component.scss']
})
export class ReserverSalleComponent implements OnInit {

  public form!: FormGroup;
  public id!: number;


  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ReserverSalleComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
    private reservationService: ReservationService
    ) { 
      this.id = data.id;
    }

  ngOnInit() {
    
    this.form = this.fb.group({
      description: new FormControl('', [Validators.required]),
      date: new FormControl('', [Validators.required]),
    });
  }

  save() {
    const description = this.form.get('description')?.value;
    const date = new Date(this.form.get('date')?.value);
    const reservation: Reservation = {
      description: description,
      date:  new Date(date),
      salle_id: this.id
    }

    this.reservationService.saveReservation(this.id,reservation);
    this.dialogRef.close(this.form.value);
}

close() {
    this.dialogRef.close();
}

}
