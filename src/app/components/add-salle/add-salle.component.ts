import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SallesService } from 'src/app/services/salles.service';

@Component({
  selector: 'app-add-salle',
  templateUrl: './add-salle.component.html',
  styleUrls: ['./add-salle.component.scss']
})
export class AddSalleComponent implements OnInit {

  public form!: FormGroup;

  pictures: Array<any> = [
    {file: 'Pegase.jpg', name: 'Pegase'},
    {file: 'Calliope.jpg', name: 'Calliope'},
    {file: 'Thalie.jpg', name: 'Thalie'},
    {file: 'Uranie.jpg', name: 'Uranie'},
    {file: 'Euterpe.jpg', name: 'Euterpe'},
    {file: 'Persee.jpg', name: 'Persee'}
  ]

  constructor(private fb: FormBuilder, private salleService: SallesService, private router: Router) { 
    this.form = this.fb.group({
      name: new FormControl('', [Validators.required]),
      capacity: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
    
  }

  onSubmit(){
    if(this.form.valid){
      const salle = {
        name: this.form.get('name')?.value,
        capacity: this.form.get('capacity')?.value,
        image: this.form.get('image')?.value,
      }
      this.salleService.saveSalle(salle);
      this.router.navigate(['/rooms/list'])
    }
  }


}
