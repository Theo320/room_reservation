import { AddSalleComponent } from "./add-salle/add-salle.component";
import { CardSalleComponent } from "./card-salle/card-salle.component";
import { ReservationsListComponent } from "./reservations-list/reservations-list.component";
import { ReserverSalleComponent } from "./reserver-salle/reserver-salle.component";
import { UpdateSalleComponent } from "./update-salle/update-salle.component";

export const components: any[] = [CardSalleComponent, AddSalleComponent, ReserverSalleComponent, UpdateSalleComponent, ReservationsListComponent];

export {CardSalleComponent, AddSalleComponent, ReserverSalleComponent, UpdateSalleComponent, ReservationsListComponent}