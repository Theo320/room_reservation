export interface Salle {
    id?: number;
    name: string;
    capacity: number;
    image: string 
}
