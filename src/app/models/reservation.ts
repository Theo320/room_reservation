import { Salle } from "./salle";

export interface Reservation {
    id?: number;
    description: string;
    date: Date;
    salle_id: number;
}