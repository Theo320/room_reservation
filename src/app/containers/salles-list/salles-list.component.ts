import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Salle } from 'src/app/models/salle';
import { SallesService } from 'src/app/services/salles.service';

@Component({
  selector: 'salles-list',
  templateUrl: './salles-list.component.html',
  styleUrls: ['./salles-list.component.scss']
})
export class SallesListComponent implements OnInit {

  public salles$!: Observable<Salle[]>;

  constructor(private sallesService: SallesService) { }

  ngOnInit() {
    this.salles$ = this.sallesService.getSalles();
  }

}
