import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSalleComponent, ReservationsListComponent } from './components';
import { UpdateSalleComponent } from './components/update-salle/update-salle.component';
import { SallesListComponent } from './containers/salles-list/salles-list.component';

const routes: Routes = [
  { path : 'rooms', children : [
    { path : 'list', component : SallesListComponent},
    {path: 'add', component: AddSalleComponent},
    {path: 'update/:id', component: UpdateSalleComponent},
    {path: 'reservations', component: ReservationsListComponent}


]
},
{ path : 'home', redirectTo : 'rooms/list', pathMatch : 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
